import React, {Component} from 'react';
import InfiniteScroll from 'react-infinite-scroller';

import AppHeader from './components/appHeader';
import Loading from './components/loading';
import ItemModal from './components/itemModal';

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            title: {
                part_1: 'BEER', part_2: 'GURU'
            },
            data: [],
            selectedItem: null,
            page: 1,
            perPage: 21,
            isLoading: true,
            hasMore: false,
        }
    }

    componentDidMount() {
        this.fetchBeers();
    }

    fetchBeers() {
        fetch(`https://api.punkapi.com/v2/beers?page=${this.state.page}&per_page=${this.state.perPage}`)
            .then(response => response.json())
            .then(data => this.setState({
                data,
                isLoading: false
            }));
    }

    fetchMoreBeers() {
        const page = this.state.page + 1;
        this.setState({page: this.state.page + 1});

        fetch(`https://api.punkapi.com/v2/beers?page=${page}&per_page=3`)
            .then(response => response.json())
            .then(data => this.setState(prevState => ({
                data: [...prevState.data, ...data]
            })));
    }

    render() {
        const loader = <Loading key={1}/>;

        let items = <Loading/>;
        if (this.state.data.length > 0) {
            items = [];
            this.state.data.map((beer, i) => {
                items.push(
                    <div key={i} className="item" onClick={() => this.setState({selectedItem: beer})}>
                        <img src={beer.image_url} alt={beer.name}/>
                        <div className="info">
                            <h3 className="name">{beer.name}</h3>
                            <div className="desc">{beer.tagline}</div>
                        </div>
                    </div>
                );
            });
        }

        return (
            <div className="app">
                <ItemModal details={this.state.selectedItem}/>
                <AppHeader title={this.state.title}/>
                <InfiniteScroll
                    pageStart={0}
                    initialLoad={false}
                    threshold={0}
                    loadMore={() => this.fetchMoreBeers()}
                    hasMore={true}
                    loader={loader}
                >
                    <div className="items-list">{items}</div>
                </InfiniteScroll>
            </div>
        );
    }
}

export default App;