import React from 'react';

const BestServedWith = ({best}) => (
    <ul className="best-with">
        {best.map((item, i) => {
            return (
                <li key={i}>- {item}</li>
            )
        })}
    </ul>
);

export default BestServedWith;