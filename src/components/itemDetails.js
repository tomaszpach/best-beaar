import React, {Component} from 'react';
import Measurement from './measurement';
import BestServedWith from './bestServedWith';
import ModalImg from './modalImg';
import MightLike from './mightLike';

class ItemDetails extends Component {
    render() {
        const details = this.props.details;
        return (
            <div>
                <div className="item-details">
                    <ModalImg image_url={details.image_url} name={details.name}/>

                    <div className="item-info">
                        <h2 className="name">{details.name}</h2>
                        <p className="tagline">{details.tagline}</p>
                        <Measurement ibu={details.ibu} abv={details.abv} ebc={details.ebc}/>
                        <p>{details.description}</p>
                        <p>{details.brewers_tips}</p>
                        <b>Best served with:</b>
                        <BestServedWith best={details.food_pairing}/>
                    </div>

                </div>
                <MightLike ibu={this.props.details.ibu} abv={this.props.details.abv}
                           ebc={this.props.details.ebc}/>
            </div>
        )
    }
}

export default ItemDetails;