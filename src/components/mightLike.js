import React from 'react';

class MightLike extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            related: []
        }
    }

    fetchBeers(ibu, abv, ebc) {
        let value = 5,
            ibu_gt = (parseInt(ibu) - value) > 0 ? parseInt(ibu) - value : 0,
            ibu_lt = parseInt(ibu) + value,
            abv_gt = (parseInt(abv) - value) > 0 ? parseInt(abv) - value : 0,
            abv_lt = parseInt(abv) + value,
            ebc_gt = (parseInt(ebc) - value) > 0 ? parseInt(ebc) - value : 0,
            ebc_lt = parseInt(ebc) + value;

        fetch(`https://api.punkapi.com/v2/beers?ebc_gt=${ebc_gt}&ebc_lt=${ebc_lt}&abv_gt=${abv_gt}&abv_lt=${abv_lt}&ibu_gt=${ibu_gt}&ibu_lw=${ibu_lt}`)
            .then(response => response.json())
            .then(related => this.setState({
                related
            }));
    }

    componentDidMount() {
        this.fetchBeers(this.props.ibu, this.props.abv, this.props.ebc);
    }

    render() {
        let related = this.state.related;

        if (related) {
            return (
                <div>
                    <b>You might also like:</b>
                    <div className="related">
                        {related.slice(1, 4).map((item, i) => {
                            return (
                                <div className="related-wrapper">
                                    <img src={item.image_url} alt={item.name}/>
                                    <h5>{item.name}</h5>
                                </div>
                            )
                        })}
                    </div>
                </div>
            )
        }
    }
}

export default MightLike;