import React from 'react';

const Measurement = ({ibu, abv, ebc}) => (
    <div className="measurement">
        <div><b>IBU:</b> {ibu}</div>
        <div><b>ABV:</b> {abv}</div>
        <div><b>EBC:</b> {ebc}</div>
    </div>
);

export default Measurement;