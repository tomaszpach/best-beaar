import React from 'react';

const Loading = () => (
    <div className="wrapper">
        <div className="spinner">
            <div className="dot1"/>
            <div className="dot2"/>
        </div>
    </div>
);

export default Loading;