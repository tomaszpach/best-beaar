import React, {Component} from 'react';
import ItemDetails from './itemDetails';

class ItemModal extends Component {
    constructor(props) {
        super(props);

        this.state = {
            details: null
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.details !== this.props.details) {
            this.setState({details: nextProps.details});
        }
    }

    render() {
        if (this.state.details != null) {
            return (
                <div className="modal">
                    <div className="details">
                        <span onClick={() => this.setState({details: null})}>x</span>
                        <ItemDetails details={this.state.details}/>
                    </div>
                </div>
            )
        }
        return null;
    }
}

export default ItemModal;