import React from 'react';

const ModalImg = ({image_url, name}) => (
    <img src={image_url} alt={name}/>
);

export default ModalImg;