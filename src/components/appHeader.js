import React from 'react';

const AppHeader = ({title}) => (
    <header>
        <h1>
            <span>{title.part_1}</span>{title.part_2}
        </h1>
    </header>
);

export default AppHeader;